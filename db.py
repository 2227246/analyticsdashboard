import oracledb

def init_db():
    # Initialize Oracle client libraries with corrected path
    oracledb.init_oracle_client(lib_dir="C:\Program Files (x86)\InstanClient\instantclient_21_14")


def get_connection():
    user = "TMDD_OJT"
    password = "OJT2024"
    host = "172.16.2.10"
    port = 1521
    service_name = "sludb0"

    # Create a Data Source Name
    dsn = oracledb.makedsn(host, port, service_name=service_name)
    
    try:
        # Connect to Oracle Database
        con = oracledb.connect(user=user, password=password, dsn=dsn)
        print("Successfully connected to Oracle Database")
        return con
    except oracledb.DatabaseError as e:
        error, = e.args
        print("Database error occurred:")
        print(f"Error Code: {error.code}")
        print(f"Message: {error.message}")
        raise

