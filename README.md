# Django Analytics Dashboard

This project is a Django-based web application for displaying enrollment charts for various departments over the past five years. The application uses Chart.js for rendering the charts.

## Table of Contents

- [Features](#features)
- [Technologies](#technologies)
- [Setup](#setup)
- [Usage](#usage)
- [Project Structure](#project-structure)
- [License](#license)

## Features

- Display enrollment data for different departments over a five-year period.
- Interactive chart using Chart.js.
- Dynamic data loading using Django.

## Technologies

- [Django](https://www.djangoproject.com/) - Backend framework
- [Chart.js](https://www.chartjs.org/) - JavaScript library for rendering charts
- HTML, CSS, JavaScript

## Setup

### Prerequisites

- Python 3.6 or higher
- Pip (Python package installer)

### Installation

1. **Clone the repository:**

   ```bash
   git clone https://gitlab.com/2201003/analyticsdashboard.git
   cd analytics-dashboard

2. **Install Dependencies:**

    pip install -r requirements.txt

3. **Apply migrations:**

    python manage.py migrate

4. **Run the development server:**

    cd dashboardslu
    python manage.py runserver

5. **Access the application:**

    Open your browser and go to http://127.0.0.1:8000/.

### Project Structure

analytics-dashboard/
│
├── dashboardslu/
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
│
├── main/
│   ├── migrations/
│   ├── static/
│   │   └── js/
│   │       └── charts.js
│   ├── templates/
│   │   └── main/
│   │       └── index.html
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── models.py
│   ├── tests.py
│   ├── urls.py
│   └── views.py
│
├── db.sqlite3
├── manage.py
├── requirements.txt
└── README.md