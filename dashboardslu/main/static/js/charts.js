//Bar Graph for Students Enrolled per Term and Department over the last 5 years

window.onload = function() {
    var ctxBar = document.getElementById('enrollmentBarChart').getContext('2d');
    var enrollmentBarChart;

    // Parse the JSON data from the script tag
    var schoolData = JSON.parse(document.getElementById('school-data').textContent);

    // Kulay ng candleshit
    var schoolColors = {
        'SAMCIS': 'rgba(198, 209, 44, 0.7)',  
        'SEA': 'rgba(255, 99, 132, 0.7)',     
        'STELA': 'rgba(54, 162, 235, 0.7)',   
        'SoL': 'rgba(75, 192, 192, 0.7)',    
        'SoM': 'rgba(173, 216, 230, 0.7)',   
        'SoNAHBS': 'rgba(0, 212, 162, 0.7)', 
        'SAS': 'rgba(153, 102, 255, 0.7)'     
    };


    function updateChart() {
        var selectedSchool = document.getElementById('schoolSelector').value;

        if (enrollmentBarChart) {
            enrollmentBarChart.destroy();
        }

        var bgColor = schoolColors[selectedSchool]; // Default color if no match

        enrollmentBarChart = new Chart(ctxBar, {
            type: 'bar',
            data: {
                labels: schoolData[selectedSchool].map(d => d.x),
                datasets: [{
                    label: selectedSchool + ' Enrollments',
                    data: schoolData[selectedSchool].map(d => d.y),
                    backgroundColor: bgColor,
                    borderColor: bgColor,
                    borderWidth: 2
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: true,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                onHover: (event, chartElement) => {
                    if (chartElement.length) {
                        const hoveredTerm = schoolData[selectedSchool][chartElement[0].index].x.split(' ')[0]; // Assumes terms are named like "First Semester 2019"
                        enrollmentBarChart.data.datasets[0].backgroundColor = schoolData[selectedSchool].map(d => {
                            return d.x.split(' ')[0] === hoveredTerm ? bgColor : 'rgba(255, 255, 255, 0)'; // Highlight the matching term bars
                        });
                        enrollmentBarChart.update();
                    }
                },
                onLeave: (event, chartElement) => {
                    enrollmentBarChart.data.datasets[0].backgroundColor = bgColor; // Reset to original color
                    enrollmentBarChart.update();
                }
            }
        });
    }

    document.getElementById('schoolSelector').addEventListener('change', updateChart);
}
