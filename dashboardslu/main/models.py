from django.db import models

class StudEnrolled(models.Model):
    term = models.CharField(max_length=100)

    class Meta:
        app_label = 'main'
