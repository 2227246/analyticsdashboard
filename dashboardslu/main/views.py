from django.shortcuts import render
from django.db import connection
from datetime import datetime
import json

def home(request):
    current_year = datetime.now().year
    enrolled_data = {'Y': 0, 'N': 0}
    schools_set = set()  # Set to hold unique school names
    school_data = {}

    with connection.cursor() as cursor:
        
        # Frequency of Enrolled and Unenrolled students
        cursor.execute("""
            SELECT ENROLLED, COUNT(*) 
            FROM stud_enrolled 
            WHERE CAST(SUBSTR(TERM, 1, 4) AS INTEGER) BETWEEN %s AND %s
            GROUP BY ENROLLED
        """, [current_year - 5, current_year])
        for row in cursor.fetchall():
            enrolled_data[row[0]] = row[1]

        # Query in counting the frequency per term over the last 5 years
        cursor.execute("""
            SELECT TERM_DESC, SCHOOL, COUNT(*) 
            FROM stud_enrolled 
            WHERE ENROLLED = 'Y' AND CAST(SUBSTR(TERM, 1, 4) AS INTEGER) BETWEEN %s AND %s
            GROUP BY TERM_DESC, SCHOOL
            ORDER BY MIN(SUBSTR(TERM, 1, 4)), TERM_DESC, SCHOOL
        """, [current_year - 5, current_year])
        enrolled_per_term_school = cursor.fetchall()
        for term, school, count in enrolled_per_term_school:
            schools_set.add(school)
            if school not in school_data:
                school_data[school] = []
            school_data[school].append({'x': term, 'y': count})

    context = {
        'enrolled_data': enrolled_data,
        'enrolled_per_term_school': json.dumps(school_data),  # Convert to JSON
        'unique_schools': sorted(schools_set),  # Convert set to a sorted list for consistent ordering
    }

    return render(request, 'main/index.html', context)

def view(request):
    return render(request, 'main/view_analytics.html')
