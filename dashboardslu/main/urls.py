from django.urls import path
from .views import home, view  # Ensure this is importing correctly

urlpatterns = [
    path('', home, name='home'),
    path('view/', view, name='view')
]

