var ctxBar = document.getElementById('enrollmentBarChart').getContext('2d');
var enrollmentBarChart;

var schoolColo
// Define color mappings based on provided palette

var schoolColors = {
    'SAMCIS': 'rgba(198, 209, 44, 0.900)',  // Example color
    'SEA': 'rgba(255, 99, 132, 0.8)',     // Example color
    'STELA': 'rgba(54, 162, 235, 0.8)',   // Example color
    'SoN': 'rgba(75, 192, 192, 0.8)',     // Example color
    'SoM': 'rgba(173, 216, 230, 0.8)',    // Bluish-white for School of Medicine
    'SoNAHBS': 'rgba(0, 212, 162, 0.753)', // Example color
    'SAS': 'rgba(153, 102, 255, 0.8)'     // Example color
    // Add other departments as necessary
};

function updateChart() {
    var selectedSchool = document.getElementById('schoolSelector').value;
    var schoolData = {};
    {% for data in enrolled_per_term_school %}
        if (!schoolData['{{ data.1 }}']) {
            schoolData['{{ data.1 }}'] = [];
        }
        schoolData['{{ data.1 }}'].push({x: '{{ data.0 }}', y: {{ data.2 }}});
    {% endfor %}

    if (enrollmentBarChart) {
        enrollmentBarChart.destroy();
    }

    var bgColor = schoolColors[selectedSchool];// Default color if no match

    enrollmentBarChart = new Chart(ctxBar, {
        type: 'bar',
        data: {
            labels: schoolData[selectedSchool].map(d => d.x),
            datasets: [{
                label: selectedSchool + ' Enrollments',
                data: schoolData[selectedSchool].map(d => d.y),
                backgroundColor: bgColor,
                borderColor: bgColor,
                borderWidth: 2
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: true,
            scales: {
                y: {
                    beginAtZero: true
                }
            },
            onHover: (event, chartElement) => {
                if (chartElement.length) {
                    const hoveredTerm = schoolData[selectedSchool][chartElement[0].index].x.split(' ')[0]; // Assumes terms are named like "First Semester 2019"
                    enrollmentBarChart.data.datasets[0].backgroundColor = schoolData[selectedSchool].map(d => {
                        return d.x.split(' ')[0] === hoveredTerm ? 'rgba(197, 197, 197, 0.800)' : 'rgba(255, 255, 255, 0)'; // Highlight the matching term bars
                    });
                    enrollmentBarChart.update();
                }
            },
            onLeave: (event, chartElement) => {
                enrollmentBarChart.data.datasets[0].backgroundColor = bgColor; // Reset to original color
                enrollmentBarChart.update();
            }
        }
    });
}