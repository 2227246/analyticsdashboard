import db

db.init_db()

try:
    # Get connection from db module
    con = db.get_connection()

    # Create a cursor object
    cursor = con.cursor()

    # Execute a query that retrieves at least one row (if available) to fetch the column headers
    cursor.execute("SELECT * FROM stud_enrolled WHERE ROWNUM = 1")
    column_headers = [i[0] for i in cursor.description]
    print("Column Headers:", column_headers)

    # If headers are retrieved successfully, proceed to fetch all data from the table
    if column_headers:
        cursor.execute("SELECT * FROM stud_enrolled")  # Query to fetch all data
        rows = cursor.fetchall()

        if rows:
            for row in rows:
                print(row)
        else:
            print("No data found in the table.")
    else:
        print("Failed to retrieve column headers, cannot proceed with data retrieval.")

finally:
    # Close the cursor and connection
    if 'cursor' in locals():
        cursor.close()
    if 'con' in locals():
        con.close()
